

#include "Level01.h"
#include "BioEnemyShip.h"


void Level01::LoadContent(ResourceManager *pResourceManager)
{

	// Setup enemy ships
	Texture *pTexture = pResourceManager->Load<Texture>("Textures\\BioEnemyShip.png");

	// set enemy count
	const int COUNT = 21;

	// set enemy starting position
	double xPositions[COUNT] =
	{
		0.25, 0.2, 0.3,
		0.75, 0.8, 0.7,
		0.3, 0.25, 0.35, 0.2, 0.4,
		0.7, 0.75, 0.65, 0.8, 0.6,
		0.5, 0.4, 0.6, 0.45, 0.55
	};

	// set delay counters for enemy ships
	double delays[COUNT] =
	{
		0.0, 0.25, 0.25,
		3.0, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.25, 0.25, 0.25, 0.25, 0.25,
		3.5, 0.3, 0.3, 0.3, 0.3
	};

	float delay = 5.0; // start delay
	Vector2 position;

	// for each item in the arrays until COUNT is reached
	for (int i = 0; i < COUNT; i++)
	{
		// itterate the delay to the next in the array
		delay += delays[i];
		// get position from array and apply it to the screen
		position.Set(xPositions[i] * Game::GetScreenWidth(), -pTexture->GetCenter().Y);

		// create and draw enemy, then add it to the level
		BioEnemyShip *pEnemy = new BioEnemyShip();
		pEnemy->SetTexture(pTexture);
		pEnemy->SetCurrentLevel(this);
		pEnemy->Initialize(position, (float)delay);
		AddGameObject(pEnemy);
	}

	// JRC This is where the level is loaded, will need something to pass module selection through
	Level::LoadContent(pResourceManager);
}

