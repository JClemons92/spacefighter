#include <string>
#include "ModuleMenuScreen.h"
#include "MainMenuScreen.h"
#include "GameplayScreen.h"

// Callback Functions
void OnBlinkSelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen('B'));
}

void OnShieldSelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new GameplayScreen('S'));
}

void OnBackSelect(MenuScreen* pScreen)
{
	pScreen->GetScreenManager()->AddScreen(new MainMenuScreen());
}

ModuleMenuScreen::ModuleMenuScreen()
{
	m_pTexture = nullptr;

	SetTransitionInTime(2.0f);
	SetTransitionOutTime(0.5f);

	Show(); // Show the screen
}

void ModuleMenuScreen::LoadContent(ResourceManager* pResourceManager)
{
	// Logo
	m_pTexture = pResourceManager->Load<Texture>("Textures\\MenuShip.png");
	m_texturePosition = Game::GetScreenCenter() - Vector2::UNIT_Y * 150;

	// Create the menu items
	const int COUNT = 4;
	MenuItem* pItem;
	Font::SetLoadSize(20, true);
	Font* pFont = pResourceManager->Load<Font>("Fonts\\Ethnocentric.ttf");

	SetDisplayCount(COUNT);

	enum Items { BLINK, SHIELD, BACK };
	std::string text[COUNT] = { "Blink ", "OverShield", "Back"};

	for (int i = 0; i < COUNT; i++)
	{
		pItem = new MenuItem(text[i]);
		pItem->SetPosition(Vector2(100, 100 + 50 * i));
		pItem->SetFont(pFont);
		pItem->SetColor(Color::Blue);
		pItem->SetSelected(i == 0);
		AddMenuItem(pItem);
	}

	GetMenuItem(BLINK)->SetSelectCallback(OnBlinkSelect);
	GetMenuItem(SHIELD)->SetSelectCallback(OnShieldSelect);
	GetMenuItem(BACK)->SetSelectCallback(OnBackSelect);
}

void ModuleMenuScreen::Update(const GameTime* pGameTime)
{
	MenuItem* pItem;

	// Set the menu item colors
	for (int i = 0; i < GetDisplayCount(); i++)
	{
		pItem = GetMenuItem(i);
		pItem->SetAlpha(GetAlpha());

		if (pItem->IsSelected()) pItem->SetColor(Color::Red);
		else pItem->SetColor(Color::Green);
	}

	MenuScreen::Update(pGameTime);
}

void ModuleMenuScreen::Draw(SpriteBatch* pSpriteBatch)
{
	pSpriteBatch->Begin();
	pSpriteBatch->Draw(m_pTexture, m_texturePosition, Color::White * GetAlpha(), m_pTexture->GetCenter());
	pSpriteBatch->End();

	MenuScreen::Draw(pSpriteBatch);
}